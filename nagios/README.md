# Custom nrpe checks

## How to deploy

1. create symlinks of each check script into /usr/local/lib/nagios/plugins/
2. declare the nrpe check in /etc/nagios/nrpe.d/
3. create appropriate entries in /etc/sudoers.d/nagios
4. restart service *nagios-nrpe-server*


### Example

Adding check for presence of user crontabs.

```
# Make the check script available from a "standard" file path
mkdir -p /usr/local/lib/nagios/plugins/
cd /usr/local/lib/nagios/plugins/
ln -s /srv/code/fcn-toolbox/nagios/check_user_crontabs

# Create a file ending with .cfg (important)
cat << EOF > /etc/nagios/nrpe.d/crontabs.cfg
command[check_user_crontabs]=sudo /usr/local/lib/nagios/plugins/check_user_crontabs
EOF

# If the check script needs sudo permission
visudo -f /etc/sudoers.d/nagios
```

File */etc/sudoers.d/nagios* should look like this

```
Cmnd_Alias MONITORING = some_command, \
    /usr/local/lib/nagios/plugins/check_user_crontabs, \
    ...
    some_other_command

nagios ALL = (ALL) NOPASSWD: MONITORING

```

Finally restart nrpe service:

    systemctl restart nagios-nrpe-server.service

