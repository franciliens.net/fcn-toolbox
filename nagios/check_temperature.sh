#!/bin/sh

# Petit script custom pour relever la température des processeurs
#
# On ne tient pas compte des indications "high" et "critical" émises
# par sensors, mais seulement de celles fournies en argument comme
# seuils "warning" et "critical"

# Config par défaut
WARNING_LEVEL=60
CRITICAL_LEVEL=80

# 
# Fonction d'aide
#
usage() {
	cat <<EOF
Usage :
  $0 -h	
  $0 [-w warning_temp] [-c critical_temp]

Valeurs par défaut:
  warning_temp: $WARNING_LEVEL
  critical_temp: $CRITICAL_LEVEL
EOF
}


#
# Gestion des paramètres
#
while getopts hw:c:e: f; do
	case "$f" in
		'h')
			usage
			exit
			;;

		'e')
			EXCLUSIONS="$( printf "%s\n%s" "$EXCLUSIONS" "$OPTARG" )"
			;;

		'w')
			WARNING_LEVEL=$( printf "%d" "$OPTARG" )
			;;

		'c')
			CRITICAL_LEVEL=$( printf "%d" "$OPTARG" )
			;;

		\?)
			usage
			exit 1
			;;
	esac
done

if ! which sensors >/dev/null 2>&1 ; then
	echo "UNKNOWN 'sensors' not found."
	exit 1
fi

# On lance la commande
# note: on tronque les flottants en entiers.
DATA=$( /usr/bin/sensors | sed -n "s/^\([A-Za-z0-9 ]\+\):[[:space:]]*+\?\(-\?[0-9]\+\)\.\?[0-9]*.C.*$/'\1'=\2;$WARNING_LEVEL;$CRITICAL_LEVEL;;/p" )
if [ -z "$DATA" ]; then
	echo "UNKNOWN no cpu found"
	exit 3
fi

# ...puis on lance la boucle principale
# note: en faisant sensors | while read line; on entre dans un sous-process shell et les variables deviennent donc locales.
#       D'où cette bidouille à base de redirection.
#       TODO: tester la portabilité (bash ok, dash ok)
RETURN_STATUS=0
RETURN_COMMENT="OK"
while read LINE; do
	TEMPERATURE="$( echo $LINE | sed 's/.*=\([0-9]\+\);.*/\1/' )"
	SONDE="$( echo $LINE | sed "s/^'\([^']\+\)'=.*/\1/" )"
	# Si la température est au-dessus du niveau critique, et que la sonde
	# ne fait pas partie des exclues => alerte
	if [ "$TEMPERATURE" -gt "$CRITICAL_LEVEL" ] && ! echo "$EXCLUSIONS" | grep "^$SONDE$" >/dev/null; then
		RETURN_STATUS=2
		RETURN_COMMENT="CRITICAL"
	fi
	# Idem ci-dessus + vérification que l'on n'a pas déjà levé une alerte
	# (si c'était le cas, on serait déjà en Warning, voir en critical)
	if [ "$TEMPERATURE" -gt "$WARNING_LEVEL" ] && [ "$RETURN_STATUS" -eq "0" ] && ! echo "$EXCLUSIONS" | grep "^$SONDE$" >/dev/null; then
		RETURN_STATUS=1
		RETURN_COMMENT="WARNING"
	fi
done <<EOF
$DATA
EOF

# on supprime les retours à la ligne
RETURN_PERF_DATA=$( echo $DATA )
printf "$RETURN_COMMENT %s|%s\n" "$( echo $DATA | sed 's/;[^ ]*/C/g' )" "$RETURN_PERF_DATA"
exit $RETURN_STATUS
