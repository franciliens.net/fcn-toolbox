#!/bin/sh

# Petit script custom pour sonder le nombre d'IP attribuées pour le VPN

# Config par défaut
WARNING_RANGE=250
CRITICAL_RANGE=253

PROGPATH=$( echo $0 | sed -e 's,[\\/][^\\/][^\\/]*$,,' )
REVISION="0.1"

# Stop at the first non-catched error
set -e

# Include check_range()
. $PROGPATH/utils.sh

# 
# Fonction d'aide
#
usage() {
	cat <<EOF
Usage :
  $0 -h	
  $0 [-w warning_range] [-c critical_range]

Valeurs par défaut:
  warning_range: $WARNING_RANGE
  critical_range: $CRITICAL_RANGE
EOF
}

#
# Gestion des paramètres
#
while getopts hw:c:W:C: f; do
	case "$f" in
		'h')
			usage
			exit
			;;

		'w')
			WARNING_RANGE="$OPTARG"
			;;

		'c')
			CRITICAL_RANGE="$OPTARG"
			;;

		\?)
			usage
			exit 1
			;;
	esac
done


#
# Lancement de la commande
#
# Note : on lance les traitements "sûrs" (décompte) ensuite pour
#        bien capturer un éventuel échec de la commande
#        principale.
#        En outre, grep retourne un code d'erreur si aucune
#        occurrence n'est trouvée.
RESULT="$( cat /etc/openvpn/ccd/*|awk '{print $2}'|sed -n 's/.*\.//p' | sort -nu | wc -l)"

# Si la commande ne s'est pas correctement executée,
# on renvoie unknown
if [ "$?" -ne 0 ]; then
	echo "UNKNOWN : error at command launch : $RESULT"
	exit $STATE_UNKNOWN
fi

# Ventilation selon valeur
RETURN_STATUS=$STATE_OK
RETURN_OUTPUT="OK ($RESULT)"
if check_range "$RESULT" "$CRITICAL_RANGE"; then
	RETURN_STATUS=$STATE_CRITICAL
	RETURN_OUTPUT="CRITICAL ($RESULT/$CRITICAL_RANGE)"
elif check_range "$RESULT" "$WARNING_RANGE"; then
	RETURN_STATUS=$STATE_WARNING
	RETURN_OUTPUT="WARNING ($RESULT/$WARNING_RANGE)"
fi

# Affichage final
printf "%s | val=%d;%s;%s\n" "$RETURN_OUTPUT" "$RESULT" "$WARNING_RANGE" "$CRITICAL_RANGE"
exit $RETURN_STATUS
