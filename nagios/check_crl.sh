#!/bin/sh

# Small script to survey CRL
# GPL v3+

# Default values
# Warning : 7 days - 10 years
# (juste because more than 10 years is really far stretched and might be a manipulation error)
RANGE_WARNING="7:3650"
# Critical : 3 days
RANGE_CRITICAL="3:"

# Output
OUTPUT_EXIT_STATUS=0
OUTPUT_DETAIL_WARNING=""
OUTPUT_DETAIL_CRITICAL=""
#OUTPUT_PERFDATA=""

PROGPATH=$( echo $0 | sed -e 's,[\\/][^\\/][^\\/]*$,,' )
REVISION="0.2"

# Stop at the first non-catched error
set -e

# Include check_range()
. $PROGPATH/utils.sh

#
# Help function
#
usage() {
	cat <<EOF
Usage :
  $0 [-w warning_range] [-c critical_range] -f file.crl [[-w...] -f file.crl ] ...

Thresholds in days.

Note: Since the file is checked against the lastest ranges given, order
      of the arguments are important.

Default values:
	warning_range:	$RANGE_WARNING
	critical_range:	$RANGE_CRITICAL
EOF
}

check_range_syntax() {
	check_range 0 "$1" >/dev/null 2>&1
	if [ "$?" -eq "2" ]; then
		return 1
	fi
	return 0
}

# Some early checks
if ! which openssl >/dev/null 2>&1 ; then
	echo "UNKNOWN 'openssl' not found."
	exit $STATE_UNKNOWN
fi

#
# Parameters management
#
while getopts hw:c:f: OPT; do
	case "$OPT" in
		'h')
			usage
			exit
			;;

		'w')
			if check_range_syntax "$OPTARG" >/dev/null; then
				RANGE_WARNING="$OPTARG"
			else
				echo "UNKNOWN: invalid range : $OPTARG"
				exit $STATE_UNKNOWN
			fi
			;;

		'c')
			if check_range_syntax "$OPTARG" >/dev/null; then
				RANGE_CRITICAL="$OPTARG"
			else
				echo "UNKNOWN: invalid range : $OPTARG"
				exit $STATE_UNKNOWN
			fi
			;;

		'f')
			# I'm not very proud of this one : aesthetically speaking, treatments
			# should not be done during params management :)
			CRL_FILE="$OPTARG"
			if [ ! -f "$CRL_FILE" ]; then
				echo "UNKNOWN: inexistent file : $CRL_FILE"
				exit $STATE_UNKNOWN
			fi

			# Extract time left, in seconds
			EXPIRATION_DATE="$( openssl crl -noout -text -in "$CRL_FILE" | sed -n "s/^[[:space:]]\+Next Update: \(.*\)$/\1/p" )"
			if [ -z "$EXPIRATION_DATE" ]; then
				echo "UNKNOWN: couldn't get expiration date."
				exit $STATE_UNKNOWN
			fi
			TIME_LEFT=$(( ( $( date --date="$EXPIRATION_DATE" +%s ) - $( date +%s ) ) / 86400 ))

			# Check time left against range
			if check_range "$TIME_LEFT" "$RANGE_CRITICAL"; then
				OUTPUT_EXIT_STATUS=$STATE_CRITICAL
				OUTPUT_DETAIL_CRITICAL="$OUTPUT_DETAIL_CRITICAL $CRL_FILE ($TIME_LEFT days left)"
			elif check_range "$TIME_LEFT" "$RANGE_WARNING"; then
				if [ "$OUTPUT_EXIT_STATUS" -eq 0 ]; then
					OUTPUT_EXIT_STATUS=$STATE_WARNING
				fi
				OUTPUT_DETAIL_WARNING="$OUTPUT_DETAIL_WARNING $CRL_FILE ($TIME_LEFT days left)"
			fi
			;;

		\?)
			usage
			exit 1
			;;
	esac
done

if [ -z "$CRL_FILE" ]; then
	echo "UNKNOWN: no file tested."
	exit $STATE_UNKNOWN
fi

case "$OUTPUT_EXIT_STATUS" in
	'0')
		printf "OK"
		;;
	'1')
		printf "WARNING %s" "$OUTPUT_DETAIL_WARNING"
		;;
	'2')
		printf "CRITICAL %s" "$OUTPUT_DETAIL_CRITICAL"
		;;
	*)
		printf "UNKNOWN"
		;;
esac

# Perfdata
#printf "|%s\n" "$OUTPUT_PERFDATA"
printf "\n"

# Exit with return status
exit $OUTPUT_EXIT_STATUS
