fcn-toolbox
===========

Outils créés par et pour franciliens.net.

## Outils

### ccoop-resplit

Une moulinette pour générer des relevés de compte mensuels au format CSV
à partir de fichiers CSV désorganisés exportés depuis l'interface
Web du Crédit Coopératif. Il n'est pas impossible que ça fonctionne
également pour d'autres banques.

Dépendances: python3

### fcn-backup

Cet outil est remplacé par [concierge-backup](https://code.ffdn.org/guillaume/concierge). 

### fcn-dolibarr

Intéragit avec Dolibarr.

Commande disponibles: get-subscribers

Dépendances: python3, psycopg2

### fcn-ispdb

Génére un fichier au [format ISP](https://db.ffdn.org/format) en utilisant les données présentes dans Dolibarr.

### fcn-sympa

Intéragit avec Sympa.

Commande disponibles: import

Dépendances: perl, sympa
