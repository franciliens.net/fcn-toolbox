#!/bin/bash

logfile=/var/log/backup-image.log
log() {
  echo "[$(date '+%F %H:%M')] $@" >> ${logfile}
}

log "--- clean backup start ---"

REP=/var/backups/fcn
ls $REP |while read f ; do
  echo $f |egrep  .*\.[0-9]{4}-[0-9]{2}-[0-9]{2}\..*|sed 's/\.[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}\..*//'
done|sort|uniq|
while read g ; do
  n=`ls -lrt $REP/$g.[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9].*|wc -l`
  log "$n backups found for $g"
  while test $n -ge 3 ; do
    ERASE=`ls -lrt $REP/$g.[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9].*|head -1|sed 's/.* //'`
    rm -f $ERASE
    log "Removed $ERASE"
    n=`ls -lrt $REP/$g.[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9].*|wc -l`
  done
done

log "--- clean backup end ---"

