#!/bin/bash

# script user-del.sh

[ $# -ne 1 ] && {
  echo "usage: $0 nom_user"
  exit
}

vpnclient=$1
# set -x
# ccd file
if test -f /etc/openvpn/ccd/${vpnclient} ; then
  echo "effacer le fichier ccd"
  rm -f /etc/openvpn/ccd/${vpnclient}
else
  echo "le fichier /etc/openvpn/ccd/${vpnclient} n'existe pas"
fi

# certificate
if test -f /etc/openvpn/easy-rsa/keys/${vpnclient}.crt ; then
  echo "certificat bien conserve"
else
  echo "attention, pas de certificat"
fi

cd /etc/openvpn/easy-rsa/
source ./vars
./revoke-full ${vpnclient}
cd -

chgrp openvpn /etc/openvpn/easy-rsa/keys/crl.pem                                                                                                         

# nettoyer 
for ext in conf cube ; do
  [ -f /etc/openvpn/private-client-conf/vpnclient-${vpnclient}.$ext ] && rm -f /etc/openvpn/private-client-conf/vpnclient-${vpnclient}.$ext
done

if test `hostname` = "vm-styx"
then
  REMOTE="replicator@vm-limbo.franciliens.net"
  echo "Syncing with $REMOTE"
  /srv/code/replicator-openvpn transfer $REMOTE:/home/replicator/openvpn
  ssh $REMOTE /srv/code/replicator-openvpn install /home/replicator/openvpn
elif test `hostname` = "vm-limbo"
then
  REMOTE="replicator@vm-styx.franciliens.net"
  echo "Syncing with $REMOTE"
  /srv/code/replicator-openvpn transfer $REMOTE:/home/replicator/openvpn
  ssh $REMOTE /srv/code/replicator-openvpn install /home/replicator/openvpn
else
  echo "Syncing aborted. Unknown machine `hostname`" >&2
  exit 1
fi
