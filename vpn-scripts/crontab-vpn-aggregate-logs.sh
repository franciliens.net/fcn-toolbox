#!/bin/sh

FIND_OPTIMISATION=""

if [ -z "$1" ]; then
	# Détermination automatique du mois à rechercher = le mois dernier.
	# - openvpn écrit ses noms de mois en anglais
	# - on retire 20j à la date actuelle pour avoir le mois précédent
	MONTH_INITIALS="$( LANG=en_US date --date=@"$(( $( date +%s ) - 86400*20 ))" +%b )"
	     MONTH_ISO="$( LANG=en_US date --date=@"$(( $( date +%s ) - 86400*20 ))" +%Y-%m )"
	FIND_OPTIMISATION="-mtime -60"
else
	MONTH_INITIALS="$( LANG=en_US date --date="$1" +%b )"
	     MONTH_ISO="$( LANG=en_US date --date="$1" +%Y-%m )"
fi
OUTPUT_FILE="/var/log/openvpn/aggregats/openvpn-aggreg-$MONTH_ISO.log"

if [ "$( echo "$MONTH_ISO" | sed -n '/^[0-9]\{4\}-[0-9]\{2\}$/p' )" = "" ]; then
	echo "ERREUR: souci de date." >&2
	exit 1
fi


(
	echo "$MONTH_ISO"
	find /var/log/openvpn/ -type f $FIND_OPTIMISATION -iname "*.log*" -print0 | xargs -0 zgrep "^$MONTH_INITIALS" | sed -n 's/.*\]: \([a-zA-Z0-9_\.\@\-]\+\)\/[0-9].*/\1/p' | sort | uniq -c
) > "$OUTPUT_FILE"
