#!/bin/sh

#######################################################################
##   .    Work in progress
##  / \   Ce script permet de refabriquer les fichier openvpn et cube.
## /_!_\  Il est déjà fonctionnel mais peu testé/documenté.
#######################################################################

# Adapté à partir du script de création de nouveau compte VPN
# /etc/openvpn/user-add.sh

# Dossier parent de easy-rsa contenant notamment
# les différents exécutables de gestion de la PKI
# ainsi que les données de la PKI elle-même.
EASYRSA_DIR="/etc/openvpn/easy-rsa"

# Dossier contenant les fichiers de configuration openvpn et cube
CONF_DIR="/etc/openvpn/private-client-conf"
# Dossier des configurations des IPv4 affectées aux comptes VPN
CCD_DIR="/etc/openvpn/ccd"

# On attend exactement 1 argument : le nom du compte VPN.
[ $# -ne 1 ] && {
  echo "Usage: " >&2
  echo " $0 CTxxxx-xxxx" >&2
  echo "or" >&2
  echo " $0 demo-xxxxxxx" >&2
  exit 1
}
account_name=$1

# Chemins des fichiers conf et cube à créer
tmp_dir=$(mktemp -d /tmp/regen-vpnconfig-$(date +%Y-%m-%d)-XXXXX)
conf_new="${tmp_dir}/vpnclient-${account_name}.ovpn"
cube_new="${tmp_dir}/vpnclient-${account_name}.cube"

# Chemins des fichiers conf et cube existants (pour comparaison)
conf_orig="${CONF_DIR}/vpnclient-${account_name}.ovpn"
cube_orig="${CONF_DIR}/vpnclient-${account_name}.cube"

## Vérification que le compte indiqué existe.
if [ ! -e "/etc/openvpn/ccd/${account_name}" -o ! -e "/etc/openvpn/easy-rsa/keys/${account_name}.key" ];
then
  echo "Account not found: '${account_name}'" >&2
  echo "One of these files is missing:
/etc/openvpn/easy-rsa/keys/${account_name}.key
/etc/openvpn/easy-rsa/keys/${account_name}.crt
/etc/openvpn/ccd/${account_name}" >&2
  exit 2
fi

##########################################################################
## Création du fichier de config vpnclient
cp /etc/openvpn/vpnclient.conf.template $conf_new

# Inclusion des certificats directement dans le fichier de config openvpn
echo "<ca>" >>$conf_new
cat /etc/openvpn/easy-rsa/keys/ca.crt >>$conf_new
echo "</ca>" >>$conf_new
echo "<cert>" >>$conf_new
sed -n '/BEGIN CERTIFICATE/,/END CERTIFICATE/ p' ${EASYRSA_DIR}/keys/${account_name}.crt >> $conf_new
echo "</cert>" >>$conf_new
echo "<key>" >>$conf_new
cat ${EASYRSA_DIR}/keys/${account_name}.key >>$conf_new
echo "</key>" >>$conf_new
echo "<tls-auth>" >>$conf_new
cat /etc/openvpn/private/vpn-ta.key >>$conf_new
echo "</tls-auth>" >>$conf_new

## Création du fichier de config au format .cube pour labriqueinter.net
# Note : IPv6 en phase de test : je pense qu'il n'y a pas de risque à donner un .cube
#        incluant des DNS ipv6 à un abonné IPv4-only. J'espère ne pas me tromper...
#        Charly - 2019-12-08
cat > $cube_new << EOF
{
  "server_name": "79.143.250.4",
  "server_port": "1194",
  "server_proto": "udp",
  "dns0": "79.143.250.1",
  "dns1": "80.67.169.40",
  "dns2": "2001:678:938::53:1",
  "dns3": "2001:678:938::53:2",
  "crt_server_ca": "`sed -n '/<ca>/,/<\/ca>/ p'     $conf_new | sed -n '/BEGIN CERTIFICATE/,/END CERTIFICATE/ p' |tr '\n' '|'`",
  "crt_client_key": "`sed -n '/<key>/,/<\/key>/ p'  $conf_new | sed -n '/BEGIN PRIVATE KEY/,/END PRIVATE KEY/ p' |tr '\n' '|'`",
  "crt_client": "`sed -n '/<cert>/,/<\/cert>/ p'    $conf_new | sed -n '/BEGIN CERTIFICATE/,/END CERTIFICATE/ p' |tr '\n' '|'`",
  "crt_client_ta": "`sed -n '/<tls-auth>/,/<\/tls-auth>/ p' $conf_new | sed -n '/BEGIN OpenVPN/,/END OpenVPN/ p' |tr '\n' '|'`",
  "openvpn_add": ["client","dev tun","compress lzo"],
  "openvpn_rm": ["tun-ipv6","pull","nobind","comp-lzo adaptive", "ns-cert-type server", "route-ipv6 2000::/3","redirect-gateway def1 bypass-dhcp","tls-client"]
}
EOF

if [ "z${SUDO_USER}" != "z" ]
then
  chown -R ${SUDO_USER}: ${tmp_dir}
fi


echo "Les nouveaux fichiers de configuration openvpn et cube sont générés dans ${tmp_dir}.
Les anciens fichiers sont dans ${CONF_DIR}.
"
