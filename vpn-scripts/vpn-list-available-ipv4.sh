#! /bin/bash

## Recherche d'une IPv4 publique disponible à affecter au compte VPN
# le subnet est 79.143.250.64/255.255.255.192,
# la gateway est 79.143.250.65
# le broadcast est 79.143.250.127
# donc :

IP_START=66
IP_END=126
IP=$IP_START

USED=$(mktemp /tmp/openvpn_ipv4_used_$(date '+%F')_XXXX.list)
FULL_RANGE=$(mktemp /tmp/openvpn_ipv4_range_$(date '+%F')_XXXX.list)

cat /etc/openvpn/ccd/* | grep -v '^\s*#' | awk '{print $2}' | grep ^79\.143 | sort -nu -k 4 -t . > ${USED}
for i in $(seq ${IP_START} ${IP_END}); do
   echo "79.143.250.${i}" >> ${FULL_RANGE}
done

diff -U0 ${USED} ${FULL_RANGE} | grep '^+79' | sed 's/^+//'

rm ${USED}
rm ${FULL_RANGE}
