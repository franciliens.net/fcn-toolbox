#!/bin/sh


# Ce script sert à allouer une bande passante minimale à chaque abonné
# VPN tout en laissant la possibilité de consommer ce qui reste
# disponible.
#
# On part du principe que Franciliens n'alloue qu'un bloc IPv4 (+ 1
# bloc IPv6) et qu'on numérote les classid de tc avec le dernier nombre
# de l'IPv4, pratique vu qu'il est utilisé pour l'IP "tête de pont" IPv6
# ainsi que (je l'espère) le futur bloc /64.
# Illustration :
# 79.143.250.94 -> aura l'IPv6 2001:678:938:100::94 et le classid 94
#
# TODO :
# - ajouter le futur bloc IPv6 par défaut assigné à chaque abonné.
# - Reste à voir ce qu'on fait en cas d'allocation arbitraire d'un
#   bloc IPv6 (par exemple si un abonné demande un /48, comment le
#   rattache-t-on à sa class et donc son débit global).
# - trouver un moyen de consolider la bande passante totale sur les
#   2 instances UDP/1194 et TCP/443 (interfaces vpns-*). Pour
#   l'instant, la limitation se fait par interface. (C'est peut-être
#   ok : OpenVPN est mono-threadé mais on est apparemment sur une
#   machine dual-coeur donc si ça se trouve, 2 instances OpenVPN
#   arrivent peut-être à atteindre 2 x BANDWIDTH_TOTAL)
# - trouver un moyen de consolider entrée + sortie.
#   Pour le moment, un même abonné pourrait consommer BANDWIDTH_TOTAL
#   en entrée + BANDWIDTH_TOTAL en sortie sur le même process
#   OpenVPN.
# - avec notre astuce d'utiliser htb sur eth0 au lieu d'ingress,
#   on ne compte pas le trafic venant d'un VPN et allant vers le
#   serveur VPN. Il n'y a que les sysadmins et les trésos à faire
#   ça et ils ont bien le droit de saturer le serveur, ces petits
#   choux ;)


# Config
# Bande passante en Kbit/s
# Testé le 2020-03-30 (alors que Styx est sur Denis ?) : saturation vers 20Mbit/s
BANDWIDTH_TOTAL=20000
BANDWIDTH_ABONNE=1000  # On devrait calculer (total / nb_abonnés) mais on prend le risque d'être généreux :)

LIST_INTERFACES_DST="vpns-udp1194 vpns-tcp443"
# Petit essai : au lieu d'utiliser ingress qui est un peu limité, on
# filtre sur l'interface de "sortie" en se basant sur les IP de l'abonné.
LIST_INTERFACES_SRC="eth0"

PREFIXE_IPV4=79.143.250.
PREFIXE_IPV6_SINGLE=2001:678:938:100::
NUM_START=66
NUM_END=126  # 998 et 999 réservés

# On s'arrête à la première erreur
set -e

generate_listing_id() {
	seq "$NUM_START" "$NUM_END"
}

adding_filters() {
	INTERFACE="$1"
	DIRECTION="$2" # src / dst
	NUMABONNE="$3"

	tc class  add dev "$INTERFACE" parent 999:999 classid 999:$NUMABONNE htb rate "$BANDWIDTH_ABONNE"Kbit ceil "$BANDWIDTH_TOTAL"Kbit
	tc filter add dev "$INTERFACE" parent 999: prio 1 u32 match ip  "$DIRECTION" "$PREFIXE_IPV4$NUMABONNE"        flowid 999:$NUMABONNE
	tc filter add dev "$INTERFACE" parent 999: prio 2 u32 match ip6 "$DIRECTION" "$PREFIXE_IPV6_SINGLE$NUMABONNE" flowid 999:$NUMABONNE

	# Pas bien compris pourquoi mais il est recommandé d'ajouter des
	# qdisc sous les classes. Ça marche bien sans, mais bon...
	# Et puis le SFQ a l'air d'être un truc intelligent pour distinguer
	# les flux de l'abonné sans trop manger de ressources CPU. À désactiver
	# si cela se révèle faux.
	tc qdisc add dev "$INTERFACE" parent 999:$NUMABONNE handle $NUMABONNE: sfq perturb 10
}

# In case we want to clean everything
if [ "$1" = "-c" ]; then
	for INTERFACE in $LIST_INTERFACES_DST $LIST_INTERFACES_SRC; do
		tc qdisc del dev "$INTERFACE" root 2>/dev/null || true
		tc qdisc add dev "$INTERFACE" root pfifo_fast
	done

	exit
fi

# (Ré)initialisation de toutes les interfaces
for INTERFACE in $LIST_INTERFACES_DST $LIST_INTERFACES_SRC; do
	tc qdisc del dev "$INTERFACE" root 2>/dev/null || true

	# Racine qdisc
	tc qdisc add dev "$INTERFACE" root handle 999: htb default 998

	# Classe racine - on lui affecte le BANDWIDTH_TOTAL du VPN "au cas où"
	tc class add dev "$INTERFACE" parent 999: classid 999:999 htb rate "$BANDWIDTH_TOTAL"Kbit

	# Classe par défaut - trafic hors OpenVPN - sans limite
	# (flûte, 'rate' est obligatoire...)
	tc class add dev "$INTERFACE" parent 999:999 classid 999:998 htb rate 10000Mbit
done

generate_listing_id | while read NUMABONNE; do
	# Trafic entrant dans le tunnel vers une IP de l'abonné
	for INTERFACE in $LIST_INTERFACES_DST; do
		adding_filters "$INTERFACE" "dst" "$NUMABONNE"
	done

	# Trafic venant d'une IP de l'abonné et partant vers Internet
	for INTERFACE in $LIST_INTERFACES_SRC; do
		adding_filters "$INTERFACE" "src" "$NUMABONNE"
	done
done
