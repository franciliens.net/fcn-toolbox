#!/bin/sh

#######################################################################
##   .    Work in progress
##  / \   Ce script (déjà fonctionnel) permet de retrouver rapidement
## /_!_\  les infos d'un utilisateur de VPN qui fait appel au support.
#######################################################################

# Adapté à partir du script de création de nouveau compte VPN
# /etc/openvpn/user-add.sh

# TODO
# - [X] afficher les N dernières dates de connexion
# - [ ] détecter les incohérences de configuration et proposer
#       des manipulations à faire pour corriger manuellement.
#       (par ex. pour le compte pitchum le certificat est stocké
#       dans easy-rsa/keys/pki/issued/pitchum.crt au lieu de
#       easy-rsa/keys directement)
# - [ ] pouvoir gérer les comptes "legacy", ceux dont le nom ne
#       correspond pas au contrat dans dolibarr
# - [X] possibilité d'accepter comme argument l'ipv4 publique
#       (ou même un hostname)


# Dossier parent de easy-rsa contenant notamment
# les différents exécutables de gestion de la PKI
# ainsi que les données de la PKI elle-même.
EASYRSA_DIR="/etc/openvpn/easy-rsa"

# Dossier contenant les fichiers de configuration openvpn et cube
CONFIG_DIR="/etc/openvpn/private-client-conf"
# Dossier des configurations des IPv4 affectées aux comptes VPN
CCD_DIR="/etc/openvpn/ccd"


# On attend exactement 1 argument : le nom du compte VPN.
[ $# -ne 1 ] && {
  echo "Usage: " >&2
  echo " $0 CTxxxx-xxxx" >&2
  echo "or" >&2
  echo " $0 demo-xxxxxxx" >&2
  exit 1
}

# Si l'argument est une ipv4 en 79.143.250 ou une IPv6 en 2001:678:938:100::
# on peut retrouver le accountname
if echo "$1" | egrep "^(79\.143\.250\.|2001:0?678:0?938:0?100)" >/dev/null 2>&1; then
  grep -Rl "$1" ${CCD_DIR}
  if [ $? -ne 0 ] ; then
    echo "IP non attribuée actuellement."
    exit 0
  fi
  account_name=$(basename $(grep -Rl "$1" ${CCD_DIR}))
elif [ -e "${CCD_DIR}/$1" ]; then
  account_name=$1
elif [ "$(echo "$1" | cut -c 1-2)" = "CT" ]; then
  account_name=$1
else
  # l'argument est probablement un nom/prénom/pseudo, on le cherche dans la BDD de dolibarr
  sql_query="select ct.ref, soc.nom from llx_contrat ct join llx_societe soc on ct.fk_soc = soc.rowid where soc.nom ilike '%"$1"%';" # XXX sqlinjection possible
  rs=$(psql -U dolibarr dolibarr -t -c "${sql_query}")
  if [ -n "$rs" -a $(echo "$rs" | wc -l) -eq 1 ]; then
    account_name=$(echo $rs | sed -r 's/^ *([^ ]+) *\|.*/\1/')
    echo $rs
  else
    echo "Trop de noms de client trouvés dans Dolibarr '$rs'."
    exit 2
  fi
fi


# Les fichiers conf et cube
conf_orig="${CONFIG_DIR}/vpnclient-${account_name}.conf"
cube_orig="${CONFIG_DIR}/vpnclient-${account_name}.cube"

# Les fichiers du certificat client
key_file="${EASYRSA_DIR}/keys/${account_name}.key"
crt_file="${EASYRSA_DIR}/keys/${account_name}.crt"

# Le fichier CCD (contenant notamment l'IPv4 attribuée)
ccd_file="${CCD_DIR}/${account_name}"

if [ ! -e "${ccd_file}" ]; then
  echo "Ce compte VPN n'existe pas. (Fichier '${ccd_file}' manquant)" >&2
  exit 2
fi

# Définition de fonctions helpers
red() {
  echo -n "\033[1;31m${1}\033[0m"
}
green() {
  echo -n "\033[1;32m${1}\033[0m"
#  echo -n "${1}"
}

log_warning() {
  echo "\033[1;33mWARN\033[0m ${1}"
}

warning_if_file_is_missing() {
  test -e ${1} || log_warning "Missing file: ${1}"
}

find_contact_info_in_dolibarr() {
  # Le format du nom de compte est important pour savoir où chercher
  if expr match "x$account_name" "xCT[0-9][0-9][0-9][0-9]-[0-9][0-9]*" > /dev/null
  then
    sql_query="select ct.ref as contrat, soc.nom as client, soc.email, ctdet.date_ouverture
               from llx_contrat ct 
	       join llx_contratdet ctdet on ct.rowid = ctdet.fk_contrat 
	       join llx_societe soc on ct.fk_soc = soc.rowid 
	       where ct.ref = '${1}'"
    sql_cmd="psql --expanded --tuples-only -U dolibarr -d dolibarr -c "
    # Le nom du compte correspond à un numéro de contrat dans dolibarr
    echo "Infos Dolibarr :"
    ${sql_cmd} "${sql_query}"
  elif expr match "x$account_name" "xdemo-..*" > /dev/null
  then
    # Le nom du compte correspond à un compte de test (aucune info dans dolibarr)
    : # no-op
  else
    # Le nom du compte correspond à un ancien compte... Là faut aller cherche l'info à la main.
    echo "La recherche d'infos dans dolibarr doit se faire manuellement pour le compte ${1}"
  fi 
}

is_currently_connected() {
  grep -q ${1} /run/openvpn/server-udp-1194.status || grep -q ${1} /run/openvpn/server-tcp-443.status
}

print_current_connection_status() {
  res=$(cat /run/openvpn/server-udp-1194.status | grep "^${1}")
  if [ "z" = "z${res}" ] ; then
    res=$(cat /run/openvpn/server-tcp-443.status | grep "^${1}")
    proto="tcp-443"
  else
    proto="udp-1194"
  fi

  # Split with /bin/sh (cf. http://xpt.sourceforge.net/techdocs/nix/shell/gsh09-SyntaxAndUsages/ar01s17.html )
  OLD_IFS=$IFS    # save internal field separator
  IFS=","         # set it to '+'
  set -- $res     # make the result positional parameters
  IFS=$OLD_IFS    # restore IFS
  ipsource=$2
  since=$5

  echo -n "État actuel : "
  if [ "z" = "z${res}" ] ; then
    echo "$(red "VPN INACTIF")"
  else
    echo "$(green "VPN ACTIF") sur ${proto}
  depuis ${since}
  à partir de l'IP ${ipsource}"
# version très colorée ci-dessous
    #    echo "$(green "VPN ACTIF")\033[1;30m sur \033[1;35m${proto}\033[1;30m, depuis \033[1;35m${since}'\033[1;30m à partir de \033[1;35m${ipsource}\033[0m"
  fi
}

print_last_connections() {
	echo "Dernières apparitions dans les logs :"
	ls -1tr /var/log/openvpn/openvpn.log* | tail | xargs zcat -f | grep "Peer Connection Initiated" | grep "\[$1\]" | sed -r 's/vm-styx/styx/'| sed -r 's/^(.*) styx (ovpn-server.*)\[[0-9]+\]: [^[]*\[([^]]+)\] .*\[AF_INET6]::ffff:(.*):[0-9]+ \(.*/[\1] \2 [\3] \4/g' | tail | sed 's/^/  /'

	echo "Dernières lignes dans les aggregats de logs :"
	grep -w " $1" /var/log/openvpn/aggregats/* | tail | sed 's/^/  /'
	return 0
}

guess_yunohost_domain_name() {
  IP=$1
  http_response=$(curl --max-time 5 -skI https://${IP}/yunohost/sso/)
  if [ $? -ne 0 ]; then
    log_warning "Échec de connexion à https://${IP}/yunohost/sso/"
    return 1
  fi
  location_header="$(echo "${http_response}" | grep ^location)"
  if [ "${location_header}" ]; then
    res=$(echo "${location_header}" | sed -r 's;^location: ?https://([^/]+)/yunohost/sso/.*;\1;')
  else
    res="Non détecté. Essayez manuellement en visitant https://${IP}/"
  fi
  echo -n "${res}"
}


#######################################################################
## Début réel d'exécution des diagnostics
#######################################################################

warning_if_file_is_missing ${ccd_file}
warning_if_file_is_missing ${key_file}
warning_if_file_is_missing ${crt_file}

print_current_connection_status ${account_name}
echo ""

ipv4="$(cat ${ccd_file} | sed -n 's/^[[:space:]]*ifconfig-push[[:space:]]\+\([0-9\.]\+\)[[:space:]]\+.*/\1/p' )"
ipv6="$(cat ${ccd_file} | sed -n 's/^[[:space:]]*ifconfig-ipv6-push[[:space:]]\+\([0-9a-fA-F:]\+\)\/[0-9]\+/\1/p' )"
echo "Adresse IPv4 publique : ${ipv4}"
echo "Adresse IPv6 frontale : ${ipv6}"
echo "Plage IPv6 : (todo)"
reverse_dns=$(dig +short -x ${ipv4})
echo "Reverse DNS IPv4:       ${reverse_dns}"
if [ -n "$ipv6" ]; then
	reverse_dns=$(dig +short -x ${ipv6})
	echo "Reverse DNS IPv6 frontale : ${reverse_dns}"
fi
echo "Délégation reverse IPv6 : (todo ?)"

if is_currently_connected "${account_name}" ; then
  detected_domain=$(guess_yunohost_domain_name ${ipv4})
  echo "Domaine auto-détecté:   ${detected_domain}"
  if [ "${detected_domain}" -a "${detected_domain}." != "${reverse_dns}" ]; then
    log_warning "Modifier le reverse DNS pour permettre l'auto-hébergement mail."
  fi
fi
echo ""

find_contact_info_in_dolibarr ${account_name}

print_last_connections ${account_name}
