#!/bin/sh

#######################################################################
##   .    Work in progress
##  / \   Ce script est début de réécriture du script user-add.sh.
## /_!_\  Il est déjà fonctionnel mais peut encore être simplifié.
#######################################################################

# Copié et modifié à partir du script original /etc/openvpn/user-add.sh

# Dossier parent de easy-rsa contenant notamment
# les différents exécutables de gestion de la PKI
# ainsi que les données de la PKI elle-même.
EASYRSA_DIR="/etc/openvpn/easy-rsa"


# On attend exactement 1 argument : le nom du compte à créer.
[ $# -ne 1 ] && {
  echo "Usage: " >&2
  echo " $0 CTxxxx-xxxx" >&2
  echo "Add a demo account: " >&2
  echo " $0 demo-xxxxxxx" >&2
  exit 1
}

account_name=$1

## Vérification que ce compte n'a pas déjà été créé auparavant.
if [ -e "/etc/openvpn/ccd/$1" ];
then
  echo "Cannot create account. Account already exists: '$1'" >&2
  exit 2
fi

## Vérification du format du nom du compte à créer (demo-xxxx ou CT-xxxx-xx)
if expr match "x$account_name" "xCT[0-9][0-9][0-9][0-9]-[0-9][0-9]*" > /dev/null
then
  echo Creating subscriber VPN account
elif expr match "x$account_name" "xdemo-..*" > /dev/null
then
  echo Creating demo VPN account
else
  echo "Error: bad account name '$account_name'" >&2
  exit 1
fi

[ -d /etc/openvpn/private-client-conf ] || mkdir /etc/openvpn/private-client-conf

CONF="/etc/openvpn/private-client-conf/vpnclient-${account_name}.ovpn"
CUBE="/etc/openvpn/private-client-conf/vpnclient-${account_name}.cube"

echo " creation du fichier de configuration $CONF" 
echo " creation du fichier de configuration $CUBE" 

#######################################################################
## Recherche d'une IPv4 publique disponible à affecter au compte VPN
# le subnet est 79.143.250.64/255.255.255.192,
# la gateway est 79.143.250.65
# le broadcast est 79.143.250.127
# donc :
IP_START=66
IP_END=126
IP=$IP_START
TEMP_FILE=/tmp/openvpn_ip_used

>$TEMP_FILE
cat /etc/openvpn/ccd/* | awk '{print $2}' | sed -n 's/.*\.//p' | sort -nu > $TEMP_FILE
N=`wc -l $TEMP_FILE|awk '{print $1}'`

if test $((${IP_END}-${IP_START}-${N})) -gt 0 ; then
  echo "il reste des ip"
else
  echo "plus d'ip disponible"
  exit
fi

while read IP_USED ; do
  if test $IP -eq $IP_USED ; then
      IP=`echo 1+$IP|bc -l`
  fi
done < $TEMP_FILE

IP6="2001:678:938:1$( printf "%02x" "$IP" )::0/64"

cat <<EOF >/etc/openvpn/ccd/${account_name}
ifconfig-push 79.143.250.$IP 255.255.255.192
ifconfig-ipv6-push ${IP6}

# Délégation de préfixe IPv6:
iroute-ipv6 ${IP6}

# Variable d'environnement custom, potentiellement utilisée par la brique pour s'auto-configurer, mais ça reste à prouver.
# https://wiki.ldn-fai.net/wiki/OpenVPN_Server_Tutorial#User_configuration
push "setenv-safe DELEGATED_IPV6_PREFIX ${IP6}"
EOF


##########################################################################
## Création du certificat dans la PKI openvpn
cd ${EASYRSA_DIR}
. ./vars
./pkitool ${account_name}
cd -

##########################################################################
## Création du fichier de config vpnclient
cp /etc/openvpn/vpnclient.conf.template $CONF

# --- inclusion des certificats dans le fichier
echo "<ca>" >>$CONF
cat /etc/openvpn/easy-rsa/keys/ca.crt >>$CONF
echo "</ca>" >>$CONF
echo "<cert>" >>$CONF
cat ${EASYRSA_DIR}/keys/${account_name}.crt  >>$CONF
echo "</cert>" >>$CONF
echo "<key>" >>$CONF
cat ${EASYRSA_DIR}/keys/${account_name}.key >>$CONF
echo "</key>" >>$CONF
echo "<tls-auth>" >>$CONF
cat /etc/openvpn/private/vpn-ta.key >>$CONF
echo "</tls-auth>" >>$CONF

## Création du fichier de config au format .cube pour labriqueinter.net
cat > $CUBE << EOF
{
  "server_name": "79.143.250.4",
  "server_port": "1194",
  "server_proto": "udp",
  "dns0": "79.143.250.1",
  "dns1": "80.67.169.40",
  "dns2": "2001:678:938::53:1",
  "dns3": "2001:678:938::53:2",
  "crt_server_ca": "`sed -n '/<ca>/,/<\/ca>/ p'     $CONF | sed -n '/BEGIN CERTIFICATE/,/END CERTIFICATE/ p' |tr '\n' '|'`",
  "crt_client_key": "`sed -n '/<key>/,/<\/key>/ p'  $CONF | sed -n '/BEGIN PRIVATE KEY/,/END PRIVATE KEY/ p' |tr '\n' '|'`",
  "crt_client": "`sed -n '/<cert>/,/<\/cert>/ p'    $CONF | sed -n '/BEGIN CERTIFICATE/,/END CERTIFICATE/ p' |tr '\n' '|'`",
  "crt_client_ta": "`sed -n '/<tls-auth>/,/<\/tls-auth>/ p' $CONF | sed -n '/BEGIN OpenVPN/,/END OpenVPN/ p' |tr '\n' '|'`",
  "openvpn_add": ["client","dev tun","compress lzo"],
  "openvpn_rm": ["tun-ipv6","pull","nobind","comp-lzo adaptive", "ns-cert-type server", "route-ipv6 2000::/3","redirect-gateway def1 bypass-dhcp","tls-client"]
}
EOF

echo "le fichier de configuration à transmetre à l'adhérent est $CONF"
echo "le fichier cube à transmettre à l'adhérent est $CUBE"

## Synchronisation des comptes avec le serveur VPN de redondance.
if test `hostname` = "vm-styx"
then
  REMOTE="replicator@vm-limbo.franciliens.net"
  echo "Syncing with $REMOTE"
  /srv/code/replicator-openvpn transfer $REMOTE:/home/replicator/openvpn
  ssh $REMOTE /srv/code/replicator-openvpn install /home/replicator/openvpn
elif test `hostname` = "vm-limbo"
then
  REMOTE="replicator@vm-styx.franciliens.net"
  echo "Syncing with $REMOTE"
  /srv/code/replicator-openvpn transfer $REMOTE:/home/replicator/openvpn
  ssh $REMOTE /srv/code/replicator-openvpn install /home/replicator/openvpn
else
  echo "Syncing aborted. Unknown machine `hostname`" >&2
  exit 1
fi
