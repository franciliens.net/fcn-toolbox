#! /usr/bin/env python3
# Ce script indique la date de dernière connexion VPN pour chaque compte,
# en se basant sur les logs openvpn des N dernières semaines (variables WEEKS_BACK ci-dessous)
# Pour lister les 10 comptes VPN les moins utilisés :
#
#    ./vpn-last-connections.py | sort -nr | tail
#

WEEKS_BACK=40
LOGFILES_PATTERN="/var/log/openvpn/openvpn.log-*" # le tiret est important, ça garantie qu'on aura l'année dans le nom du fichier

import os, glob, gzip, re
from datetime import datetime

# Une regex pour parser les 2 type de lignes attendues :
# Dec 17 00:07:17 styx ovpn-server-udp-1194[491]: 86.245.38.51 [CT1904-0108] Peer Connection Initiated with [AF_INET6]::ffff:86.245.38.51:50389 (via ::ffff:79.143.250.4%eth0)
# Dec 16 18:40:56 styx ovpn-server-tcp-443[490]: 176.191.200.203:57320 [CT1809-0084] Peer Connection Initiated with [AF_INET]176.191.200.203:57320
regex_line=re.compile('^(.*) styx (ovpn-server.*)\[[0-9]+\]: [^[]*\[([^]]+)\] .*\[AF_INET6?](?:::ffff:)?(.*):[0-9]+.*')

def main():
    active_vpn_accounts = [os.path.basename(f) for f in glob.glob("/etc/openvpn/ccd/*")]
    res = dict()
    for logfile in sorted(glob.glob(LOGFILES_PATTERN))[-WEEKS_BACK:]:
        year = os.path.basename(logfile).split("-")[1][0:4]
        if logfile.endswith('.gz'):
            fin = gzip.open(logfile, 'rt')
        else:
            fin = open(logfile, 'rt')

        for line in fin.readlines():
            line = line.strip()
            if "Peer Connection Initiated" not in line:
                continue
            line = line.replace("vm-styx", "styx")
            m = regex_line.match(line)
            if not m:
                print("Failed to parse line: '{}'".format(line))
                continue
            date,_,ctname,_ = m.groups(1)
            if ctname not in active_vpn_accounts:
                continue
            res[ctname] = datetime.strptime(year + " " + date[0:6], "%Y %b %d")

        fin.close()
    for ctname, date in res.items():
        print("{:%Y-%m-%d} {}".format(date, ctname))


if __name__ == '__main__':
    main()
