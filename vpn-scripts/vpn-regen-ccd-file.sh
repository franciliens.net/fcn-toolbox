#!/bin/sh

CONFFILE="/etc/openvpn/ccd/$1"

if [ ! -f "$CONFFILE" ]; then
	echo "ERROR: fichier '$CONFFILE' inexistant." >&2
	echo "usage: $0 CT1234-1234" >&2
	exit 1
fi

IP="$( sed -n 's/^[[:space:]]*ifconfig-push[[:space:]]\+79.143.250.\([0-9]\+\)[[:space:]]\+.*/\1/p' "$CONFFILE" )"
if [ -z "$IP" ]; then
	echo "ERROR: 'pas réussi à récupérer l'adresse IP..." >&2
	exit 1
fi

IP6="2001:678:938:1$( printf "%02x" "$IP" )::0/64"

cat <<EOF > ${CONFFILE}
ifconfig-push 79.143.250.$IP 255.255.255.192
ifconfig-ipv6-push ${IP6}

# Délégation de préfixe IPv6:
iroute-ipv6 ${IP6}

# Variable d'environnement custom, potentiellement utilisée par la brique pour s'auto-configurer, mais ça reste à prouver.
# https://wiki.ldn-fai.net/wiki/OpenVPN_Server_Tutorial#User_configuration
push "setenv-safe DELEGATED_IPV6_PREFIX ${IP6}"
EOF

echo "Fichier recréé : ${CONFFILE}"
