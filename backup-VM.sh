#!/bin/bash
# Changes:
# - 2016-11-01 / guillaume / gzip -> lz4
# - 2016-11-24 / laurent / lz4 -> $DATE.lz4 necessaire a la rotation des backups
# - 2016-12-18 / laurent / backup sur www.wargon.org si necessaire
# - 2016-12-20 / laurent / backup sur limbo.franciliens.net si necessaire
VM_name=$1
lvm_name=$2
lvm_name_2=$3

logfile=/var/log/backup-image.log
log() {
  echo "[$(date '+%F %H:%M')] $@" >> ${logfile}
}

VG=$(lvs | awk '{print $1 " " $2}' | grep "^${VM_name} " | cut -d' ' -f2)
DATE=`date +%Y-%m-%d`
REP_BACKUP_LOC="/var/backups/fcn"
REP_BACKUP_DST="/var/backups/fcn"
IMAGE="$lvm_name.$DATE.lz4"
IMAGE_2="$lvm_name_2.$DATE.lz4"
VOL_SNAPSHOT="10G"
BWLIMIT=400 # réduit à une fraction de nos 10Mb/s lorsqu'on sort d'Absolight - cf chl-franciliens@bugness.org - 2018-09-18
HOSTNAME="$( hostname )"

# TODO : à décommenter à la fin du dev.
#exec >> $log 2>&1

for i in apollon artemis denis edward chelsea backup-franciliens.bugness.org; do
  [ "$HOSTNAME" = "$i" ] && continue
  if nc -z "$i.franciliens.net" 22 >/dev/null 2>&1; then
    break;
  fi
  i=""
done
if [ -n "$i" ]; then
  AUTRE="replicator@${i}.franciliens.net"
fi


log "------------------------------------------------"
[ $# -lt 2 ] && {
  echo "usage: $0 nom_de_machine nom_du_lvm"
  exit
}

log "Starting backup of ${VM_name}..."

log "Status of $VM_name: $(virsh domstate $VM_name)"
virsh domstate $VM_name | grep running > /dev/null
IS_VM_RUNNING_HERE=$?

[ $IS_VM_RUNNING_HERE -ne 0 ] && {
  log "VM ${VM_name} is not running here. Aborting."
  exit
}

[ $IS_VM_RUNNING_HERE -eq 0 ] && {
  log "virsh suspend $VM_name"
  # todo : utiliser plutot freeze et installer le paquet qui va bien dans la vm 
  virsh suspend $VM_name
  [ $? -ne 0 ] && { log "could not suspend $VM_name" ; exit ; }
}

log "lvcreate --snapshot --size $VOL_SNAPSHOT --name $lvm_name-snapshot /dev/$VG/$lvm_name" 
lvcreate --snapshot --size $VOL_SNAPSHOT --name $lvm_name-snapshot /dev/$VG/$lvm_name

[ -n "$lvm_name_2" ] && {
 log "lvcreate --snapshot --size $VOL_SNAPSHOT --name $lvm_name_2-snapshot /dev/$VG/$lvm_name_2" 
 lvcreate --snapshot --size $VOL_SNAPSHOT --name $lvm_name_2-snapshot /dev/$VG/$lvm_name_2
}

[ $IS_VM_RUNNING_HERE -eq 0 ] && {
  log "virsh resume $VM_name"
  virsh resume $VM_name
  [ $? -ne 0 ] && { log "could not resume $VM_name" ; exit ; }
}

log "Compressing $lvm_name into ${REP_BACKUP_LOC}/${IMAGE}..."
# not debug yet (reduction des ressources systeme avec nice et ionice)
nice -n 19 ionice -c3 dd if=/dev/$VG/$lvm_name-snapshot | \
nice -n 19 ionice -c3 lz4 | \
nice -n 19 ionice -c3 dd of=${REP_BACKUP_LOC}/${IMAGE}

[ -n "$lvm_name_2" ] && {
  log "Compressing $lvm_name_2 into ${REP_BACKUP_LOC}/${IMAGE}..."
  # not debug yet (reduction des ressources systeme avec nice et ionice)
  nice -n 19 ionice -c3 dd if=/dev/$VG/$lvm_name_2-snapshot | \
  nice -n 19 ionice -c3 lz4 | \
  nice -n 19 ionice -c3 dd of=${REP_BACKUP_LOC}/${IMAGE_2}
}

log "Removing the LV snapshot : $lvm_name-snapshot" 
lvremove  -f /dev/$VG/$lvm_name-snapshot

[ -n "$lvm_name_2" ] && {
  log "Removing the LV snapshot : $lvm_name_2-snapshot" 
  lvremove  -f /dev/$VG/$lvm_name_2-snapshot
}

log "Backuping the kvm XML config file :"
virsh dumpxml $VM_name > ${REP_BACKUP_LOC}/${VM_name}.xml

log "Backuping LVM volume sizes"
lvdisplay > ${REP_BACKUP_LOC}/lvdisplay.$HOSTNAME

if [ -n "$AUTRE" ]; then
  log "Transfering ${IMAGE} to $AUTRE ..."
  su - replicator -c "rsync --bwlimit=$BWLIMIT  ${REP_BACKUP_LOC}/${IMAGE} ${AUTRE}:${REP_BACKUP_DST}/${IMAGE}"

  [ -n "$lvm_name_2" ] && {
    log "Transfering ${IMAGE_2} to $AUTRE ..."
    # not debug yet : avec --bwlimit=128 le transfert se fait en moins de 5h
    su - replicator -c "rsync --bwlimit=$BWLIMIT  ${REP_BACKUP_LOC}/${IMAGE_2} ${AUTRE}:${REP_BACKUP_DST}/${IMAGE_2}"
  }

  # pour bien faire fonctionner le script qui efface les images
  # not debug yet :
  su - replicator -c "ssh $AUTRE touch ${REP_BACKUP_DST}/$IMAGE"
  [ -n "$lvm_name_2" ] && { su - replicator -c "ssh $AUTRE touch $IMAGE_2" ; }

  # not debug yet :
  su - replicator -c "scp ${REP_BACKUP_LOC}/${VM_name}.xml  ${AUTRE}:${REP_BACKUP_DST}"

  # not debug yet :
  su - replicator -c "scp ${REP_BACKUP_LOC}/lvdisplay.$HOSTNAME ${AUTRE}:${REP_BACKUP_DST}"
else
  log "Did not find a machine to transfer the image to."
fi

log "End of backup."

# restore :
# root@denis:~# ionice -c3 dd if=/var/backups/fcn/limbo.20xx-xx-xx.gz |ionice -c3 unlz4|ionice -c3 dd of=/dev/vg-denis/limbo

