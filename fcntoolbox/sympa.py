#!/usr/bin/python3
# Style Guide for Python Code https://www.python.org/dev/peps/pep-0008/
# Docstring Conventions https://www.python.org/dev/peps/pep-0257/

# Based upon:
# https://www.sympa.org/distribution/contrib/sympa_extract.pl

class Instance:
  def __init__(self, conn):
    self._conn = conn
  def get_editors(self, listname, robot):
    """Get the list's moderators, aka editors"""
    rows = ["editors_list"]
    cur = self._conn.cursor()
    cur.execute("""SELECT %s FROM list_table WHERE name_list LIKE %%s AND robot_list LIKE %%s""" % (",".join(rows)) , (listname, robot))
    editors = cur.fetchone()
    if editors[0] is None or len(editors[0]) == 0:
      return []
    else:
      return editors[0].split(',')
  def get_subscribers(self, listname, robot):
    rows = ["user_subscriber", "list_subscriber"]
    cur = self._conn.cursor()
    cur.execute("""SELECT %s FROM subscriber_table WHERE list_subscriber LIKE %%s AND robot_subscriber LIKE %%s""" % (",".join(rows)) , (listname, robot))
    subscribers = []
    for item in cur:
      email = item[0].lower()
      subscribers.append(email)
    return subscribers
