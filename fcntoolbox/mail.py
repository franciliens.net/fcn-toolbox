#!/usr/bin/python3
# Style Guide for Python Code https://www.python.org/dev/peps/pep-0008/
# Docstring Conventions https://www.python.org/dev/peps/pep-0257/

import re

class Aliases:
  def __init__(self, filename, domain):
    self.filename = filename
    self.aliases = dict()
    self.domain = domain.lower()
    with open(self.filename, "r") as f:
      for line in f: 
        match = re.search('^([^#:]+):([^#:]+)(?:#.*|)$', line)
        if match: 
          alias = match.group(1).strip() + '@' + self.domain
          to = match.group(2).strip()
          if '@' in to:
            self.aliases[alias] = to
          else:
            self.aliases[alias] = to + '@' + self.domain
  def get_alias(self, key):
    return self.aliases[key.lower()]
  def get_aliases(self):
    return self.aliases
  def __contains__(self, key):
    return key.lower() in self.aliases
  def __getitem__(self, key):
    return self.aliases[key.lower()]
  def items(self):
    return self.aliases.items()

